// console.log("Hello World")

//3 and 4
fetch("https://jsonplaceholder.typicode.com/todos")
// use the json method from the response object to convert data retrieved into JSON Format
.then((response) => response.json())
.then((json) => json.map(({title}) => title))
.then((titles) => console.log(titles))

//5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));

//6 
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`The item ${json.title} on the list has a status of ${json.status}`));

//7 Creating
fetch("https://jsonplaceholder.typicode.com/todos/",{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: "Created To Do List Item"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//8 Updating
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		id: 1,
		dateCompleted: "Pending",
		description: "To update my todo list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//9 10 11
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		status:"Completed",
		dateCompleted:"07/09/21",
		title:"deletcus aut autem"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//12
// [SECTION] - Deleting a Post
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method:"DELETE"
});